package ae.bridgeresultat.model;

public class InvalidFieldException extends Exception {

    private String id;
    private String round;
    private String input;

    public InvalidFieldException() {
        super();
    }

    public InvalidFieldException(String id, String round, String input) {
        super();
        this.id = id;
        this.round = round;
        this.input = input;
    }

    public String getId() {
        return id;
    }

    public String getRound() {
        return round;
    }

    public String getInput() { return input; }

    @Override
    public String getMessage() {
        if (input.trim().equals("")) {
            return "I kolumn " + round + " är poängen för några lag inte angivna.\n";
        }
        return "Fel inmatning för laget \"" + id + "\" vid kolumn " + round + ".\n"
                + "(Ange ett tal eller ett minustecken).\n";
    }
}
