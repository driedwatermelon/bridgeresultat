package ae.bridgeresultat.view;

import ae.bridgeresultat.controller.Controller;

import javax.swing.*;
import java.awt.*;

public class MainButtonPanel extends JPanel {

    private Controller controller;

    private final Font BUTTON_FONT = new Font("Arial", Font.BOLD, 25);

    public MainButtonPanel(Controller controller) {
        super(new FlowLayout(FlowLayout.CENTER, 29, 30));
        this.controller = controller;
        this.setBackground(MainFrame.TOOLBAR_COLOR);
        this.add(instructionsButton());
        this.add(calculateButton());
    }

    private JButton createNewButton(String displayText) {
        JButton newButton = new JButton(displayText);
        newButton.setFont(BUTTON_FONT);
        newButton.setPreferredSize(new Dimension(400, 60));
        return newButton;
    }

    private JButton instructionsButton() {
        JButton result = createNewButton("Instruktioner");
        result.addActionListener(e -> {
            controller.showInstructions();
        });
        return result;
    }

    private JButton calculateButton() {
        JButton result = createNewButton("Räkna resultat och spara");
        result.addActionListener(e -> {
            controller.calculateResults();
        });
        return result;
    }
}
