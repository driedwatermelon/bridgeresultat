package ae.bridgeresultat.controller;

import ae.bridgeresultat.model.TableData;
import ae.bridgeresultat.model.TeamData;

import javax.swing.*;


public class JTableExtractor {

    private JTable table = null;

    public JTableExtractor() {}

    public JTableExtractor(JTable table) {
        this.table = table;
    }

    public JTable getTable() { return table; }

    public void setTable(JTable table) { this.table = table; }

    public TableData extract() {
        if (table.isEditing()) table.getCellEditor().stopCellEditing(); // Save last JTable entry to table model.

        int teamCount = 0;
        int roundCount = 0;

        // Calculate the number of teams and the number of rounds they've played.
        for (int row = 0; row < table.getRowCount(); row++){ // loop through the rows of the table

            // The first two columns are team number and name, so to extract the points, column counting begins at two.
            for (int col = 2; col < table.getColumnCount(); col++) {
                if (table.getValueAt(row, col).toString().trim().length() > 0) { // there is data in the current cell

                    // +1 compensates for index counting beginning at zero.
                    if (teamCount < row + 1) teamCount = row + 1;

                    // +1 compensates for index counting beginning at zero.
                    // -2 compensates for skipping the first two columns.
                    // -2 + 1 = -1
                    if (roundCount < col - 1) roundCount = col - 1;
                }
            }
        }


        TableData data = new TableData();

        for (int row = 0; row < teamCount; row++) {
            String number = table.getValueAt(row, 0).toString().trim();
            String name = table.getValueAt(row, 1 ).toString().trim();
            String[] points = new String[roundCount];

            for (int round = 0; round < roundCount; round++) {
                points[round] = table.getValueAt(row, round + 2).toString().trim(); // +2 compensates for skipping 2 columns.
            }
            if (emptyRecord(name, points)) continue;
            data.addRecord(new TeamData(number, name, points));
        }

        return data;
    }

    private boolean emptyRecord(String name, String[] points) {
        if (!name.isEmpty()) return false;
        for (String point : points)
            if (!point.isEmpty())
                return false;
        return true;
    }
}
