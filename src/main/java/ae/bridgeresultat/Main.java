package ae.bridgeresultat;


import ae.bridgeresultat.controller.Controller;

import javax.swing.*;
import java.awt.*;

public class Main {

	public static void main(String[] args) {
		try {
			UIManager.setLookAndFeel(UIManager.getCrossPlatformLookAndFeelClassName());
			UIManager.put("OptionPane.messageFont", new Font("Georgia", Font.PLAIN, 24));
			UIManager.put("OptionPane.buttonFont", new Font("Arial", Font.BOLD, 22));
        } catch (Exception e) {
			e.printStackTrace();
		}

        SwingUtilities.invokeLater(() -> new Controller());
	}
}
