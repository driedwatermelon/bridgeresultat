package ae.bridgeresultat.controller;

import javax.swing.*;

import ae.bridgeresultat.model.InvalidFieldException;
import ae.bridgeresultat.model.TableData;
import ae.bridgeresultat.model.TeamData;
import ae.bridgeresultat.model.filewriters.OutputWriter;
import ae.bridgeresultat.model.filewriters.XLSXWriter;
import ae.bridgeresultat.view.MainFrame;



public class Controller  {

    private MainFrame view = new MainFrame(this);
    private TableData model = new TableData();


    // Called whenever the user clicks on the instructions button.
    public void showInstructions() {
        String instructions = ""
                + "1.   Ändra Rubriken \"Bridgeprotokoll\" om du vill.\n"
                + "\n"
                + "2.   Ändra datumet om det inte stämmer (programmet fyller\n"
                + "      automatiskt i dagens datum, så det stämmer antagligen inte).\n"
                + "\n"
                + "3.   Fyll i lagens namn i kolumnen \"LAG\".\n"
                + "\n"
                + "4.   Fyll i resultaten för alla brickor i de numrerade kolumnerna. Om ett lag inte\n"
                + "      deltagit i en bricka som andra lag spelat, markera det med ett minustecken.\n"
                + "\n"
                + "5.   Tryck på räkna resultat och spara. Programmet kommer att spara\n"
                + "      resultaten i en Excel-fil som heter: " + getFileName() + "\n"
                + "      Om du vill se resultaten, öppna Excel-filen.\n"
                + "\n";
        JOptionPane.showMessageDialog(null, instructions, "Instruktioner", JOptionPane.INFORMATION_MESSAGE);
    }

    // Called whenever the user clicks on the calculate button.
    public void calculateResults() {
        JTableExtractor extractor = new JTableExtractor(view.getTable());
        model = extractor.extract();
        model.setIgnoreSymbol("-");

        try {
            model.calculateRecords();
        } catch (InvalidFieldException e) {
            e.printStackTrace();
            JOptionPane.showMessageDialog(null, e.getMessage(), "Felmeddelande", JOptionPane.ERROR_MESSAGE);
            return;
        }

        if (model.getTotalRounds() == 0) {
            JOptionPane.showMessageDialog(null,
                    "Det finns ingen data att basera uträkningen på.\n",
                    "Felmeddelande",
                    JOptionPane.ERROR_MESSAGE);
            return;
        }

        if (!model.validateColumnSums()) {
            JOptionPane.showMessageDialog(null,
                    "Kontrollräkningen av kolumnerna gav olika resultat för några kolumner.\n"
                            + "Öppna Excel-filen för att se vilka kolumner hade olika poäng.\n",
                    "Varning",
                    JOptionPane.WARNING_MESSAGE);
        }

        String[] options = new String[] {"I Mina Dokument", "På Skrivbordet", "Avbryt"};
        int response = JOptionPane.showOptionDialog(null,
                "Var vill du spara resultaten?",
                "Spara Som",
                JOptionPane.YES_NO_CANCEL_OPTION,
                JOptionPane.QUESTION_MESSAGE,
                null,
                options,
                options[0]);


        String path;

        if (response == 0) {
            path = System.getProperty("user.home") + "/Documents/" + getFileName();
        } else if (response == 1) {
            path = System.getProperty("user.home") + "/Desktop/" + getFileName();
        } else {
            return;
        }

        OutputWriter writer = new XLSXWriter(view.getTitle(), view.getDate(), model, path, getFileName());
        writer.writeFile();

        System.out.println("Results written to " + path);
    }

    public String getFileName() {
        return view.getTitle() + "_" + view.getDate().replace(".","-") + ".xlsx";
    }
}
