package ae.bridgeresultat.model.filewriters;

import ae.bridgeresultat.model.TeamData;
import ae.bridgeresultat.model.TableData;
import org.apache.poi.ss.usermodel.*;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import javax.swing.*;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

public class XLSXWriter extends OutputWriter {

    private final int LEFT_MARGIN = 0;
    private final int TOP_MARGIN = 0;
    private final int DATE_ROW_NO = TOP_MARGIN;
    private final int HEADER_ROW_NO = 1 + TOP_MARGIN;
    private final int TABLE_START_ROW_NO = 2 + TOP_MARGIN;
    private final int VALIDATE_ROW_MARGIN = 1; // Gap between table body and validation row


    public XLSXWriter(String name, String date, TableData table, String path, String fileName) {
        super(name, date, table, path, fileName);
    }

    @Override
    public void writeFile() {
        XSSFWorkbook workbook = new XSSFWorkbook();
        XSSFSheet sheet = workbook.createSheet(name);

        // Create the style and font for the date row.
        CellStyle dateStyle = workbook.createCellStyle();
        dateStyle.setAlignment(HorizontalAlignment.CENTER);
        Font dateFont = workbook.createFont();
        dateFont.setFontHeightInPoints((short) 14);
        dateFont.setItalic(true);
        dateStyle.setFont(dateFont);

        // Create the style and font for the table header.
        CellStyle headerStyle = workbook.createCellStyle();
        headerStyle.setAlignment(HorizontalAlignment.CENTER);
        Font headerFont = workbook.createFont();
        headerFont.setFontHeightInPoints((short) 14);
        headerFont.setBold(true);
        headerStyle.setFont(headerFont);

        // Create the style and font for (most of) the table body.
        CellStyle tableStyle = workbook.createCellStyle();
        tableStyle.setAlignment(HorizontalAlignment.CENTER);
        Font tableFont = workbook.createFont();
        tableFont.setFontHeightInPoints((short) 14);
        tableStyle.setFont(tableFont);

        // Create the style for the contestants column in the table body.
        CellStyle nameStyle = workbook.createCellStyle();
        nameStyle.setFont(tableFont);

        // Create the style for the win percentage column in the table body.
        CellStyle percentStyle = workbook.createCellStyle();
        percentStyle.setAlignment(HorizontalAlignment.CENTER);
        percentStyle.setDataFormat(workbook.createDataFormat().getFormat("0.00%"));
        percentStyle.setFont(tableFont);

        writeDate(sheet, dateStyle); // Write the first row, containing the date
        int columnAmount = writeHeader(sheet, headerStyle); // Write the table header.
        writeTable(sheet, tableStyle, nameStyle, percentStyle); // Write the table body.
        writeValidation(sheet, tableStyle); // Write the validation row.

        // Automatically resize all columns.
        for (int i = 0; i < columnAmount; i++) sheet.autoSizeColumn(i);

        try {
            FileOutputStream outputStream = new FileOutputStream(path);
            workbook.write(outputStream);
            workbook.close();
            JOptionPane.showMessageDialog(null, "Resultaten har sparats i filen: " + super.fileName);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
            JOptionPane.showMessageDialog(null,
                    "Resultaten kan inte sparas eftersom det redan fil med namnet\n"
                            + "\"" + super.fileName + "\" och den är öppen i ett annat\n"
                            +"program. Sannolikt har du redan gjort en uträkning med samma\n"
                            + "rubrik och datum, och därefter öppnat resultaten i Excel. Om du\n"
                            + "vill skriva över den existerande filen, stäng Excel och försök igen.\n"
                            + "Om du istället vill spara en kopia, ändra rubriken och försök igen.\n"
                            + "");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void writeDate(XSSFSheet sheet, CellStyle style) {
        Row row = sheet.createRow(DATE_ROW_NO);

        Cell cell = row.createCell(0 + LEFT_MARGIN);
        cell.setCellValue("Datum");
        cell.setCellStyle(style);

        cell = row.createCell(1 + LEFT_MARGIN);
        cell.setCellValue(super.date);
        cell.setCellStyle(style);
    }

    private int writeHeader(XSSFSheet sheet, CellStyle style) {
        Row row = sheet.createRow(HEADER_ROW_NO);
        int column = LEFT_MARGIN;

        Cell cell = row.createCell(column++);
        cell.setCellValue("LAG nr.");
        cell.setCellStyle(style);

        cell = row.createCell(column++);
        cell.setCellValue("LAG");
        cell.setCellStyle(style);


        cell = row.createCell(column++);
        cell.setCellValue("Placering");
        cell.setCellStyle(style);


        cell = row.createCell(column++);
        cell.setCellValue("Vinstprocent (%)");
        cell.setCellStyle(style);


        for (int n = 1; n <= super.table.getTotalRounds(); n++) {
            cell = row.createCell(column++);
            cell.setCellValue(n);
            cell.setCellStyle(style);
        }

        cell = row.createCell(column++);
        cell.setCellValue("Totalt");
        cell.setCellStyle(style);

        return column;
    }

    private void writeTable(XSSFSheet sheet, CellStyle defaultStyle, CellStyle nameStyle, CellStyle percentStyle) {
        int rowNo = TABLE_START_ROW_NO;
        for (TeamData team : table.getRecords()) {
            Row row = sheet.createRow(rowNo++);
            int column = LEFT_MARGIN;

            // Team Number
            Cell cell = row.createCell(column++);
            try {
                cell.setCellValue(Integer.parseInt(team.getNumber()));
            } catch (NumberFormatException e) {
                e.printStackTrace();
                cell.setCellValue(team.getNumber());
            }
            cell.setCellStyle(defaultStyle);

            // Team Name
            cell = row.createCell(column++);
            cell.setCellValue(team.getName());
            cell.setCellStyle(nameStyle);

            // Team Placement
            cell = row.createCell(column++);
            cell.setCellValue(team.getRank());
            cell.setCellStyle(defaultStyle);

            // Team Rank
            cell = row.createCell(column++);
            cell.setCellValue(team.getWinrate());
            cell.setCellStyle(percentStyle);

            // Team points for every round.
            for (int i = 0; i < team.getTotalRounds(); i++) {
                cell = row.createCell(column++);
                String score = team.getPointsEachRound()[i];

                try {
                    cell.setCellValue(Double.parseDouble(score)); // Save as number if score is specified for round.
                } catch (NumberFormatException e) {
                    e.printStackTrace();
                    cell.setCellValue(score); // Save as text if score is not specified for round (marked with minus).
                }

                cell.setCellStyle(defaultStyle);
            }

            // Total points for team.
            cell = row.createCell(column);
            cell.setCellValue(team.getTotalPoints());
            cell.setCellStyle(defaultStyle);
        }
    }

    private void writeValidation(XSSFSheet sheet, CellStyle style) {
        Row row = sheet.createRow(TABLE_START_ROW_NO + table.getRecordAmount() + VALIDATE_ROW_MARGIN);
        Cell cell = row.createCell(1 + LEFT_MARGIN);
        cell.setCellValue("\u2211 Kontroll:" );
        cell.setCellStyle(style);

        int column = 4 + LEFT_MARGIN;
        for (int i = 0; i < table.getTotalRounds(); i++) {
            cell = row.createCell(column++);
            cell.setCellValue(table.getColumnSums()[i]);
            cell.setCellStyle(style);
        }
    }
}
