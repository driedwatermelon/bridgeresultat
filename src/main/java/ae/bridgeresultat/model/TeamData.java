package ae.bridgeresultat.model;

import java.math.BigDecimal;

public class TeamData implements Comparable<TeamData> {

    private final String teamNumber;
    private final String teamName;
    private final String[] pointsEachRound;

    private double totalPoints = -1.0;
    private int roundsPlayed = -1;
    private double maxPoints = -1.0;
    private double winrate = -1.0; // Determines a teams placement.
    private int rank = -1;

    public TeamData(String teamNumber, String teamName, String[] pointsEachRound) {
        this.teamNumber = teamNumber;
        this.teamName = teamName;
        this.pointsEachRound = pointsEachRound;
    }

    public void calculatePoints(String ignoreSymbol) throws InvalidFieldException {
        totalPoints = 0;
        roundsPlayed = 0;
        for (int i = 0; i < pointsEachRound.length; i++) {
            if (pointsEachRound[i].equals(ignoreSymbol)) continue;

            try {
                pointsEachRound[i] = pointsEachRound[i].replace(",", ".");
                double points = Double.parseDouble(pointsEachRound[i]);
                totalPoints += points;
                roundsPlayed++;
            } catch (NumberFormatException e) {
                e.printStackTrace();
                throw new InvalidFieldException(teamNumber + ". " + teamName, "" + (i + 1), pointsEachRound[i]); // +1 for 0th index counting
            }
        }
    }

    public void calculateWinrate() {
        if (totalPoints < 0 && maxPoints < 0)
            throw new IllegalStateException();
        winrate =  totalPoints / maxPoints;
    }

    @Override
    public int compareTo(TeamData o) {
        return Double.compare(o.winrate, this.winrate);
    }

    @Override
    public String toString() {
        String result = teamNumber + ". " + teamName + ", rank: " + rank
                + ", winrate: " + winrate + ", total: " + totalPoints + ", rounds: ";
        for (String roundPoints : pointsEachRound) {
            result += roundPoints + ", ";
        }
        result = result.substring(0, result.length() - 2);
        return result;
    }

    public String getNumber() { return teamNumber; }

    public String getName() { return teamName; }

    public String[] getPointsEachRound() { return pointsEachRound; }

    public boolean participatedInRound(int n, String ignoreSymbol) {
        if (pointsEachRound[n].equals(ignoreSymbol)) return false;
        return true;
    }

    public double getPointsForRound(int n) {
        try {
            double points = Double.parseDouble(pointsEachRound[n]);
            return points;
        } catch (NumberFormatException e) {
            e.printStackTrace();
            return 0;
        }
    }

    public int getTotalRounds() { return pointsEachRound.length; }

    public double getTotalPoints() {
        if (totalPoints < 0)
            throw new IllegalStateException();
        return totalPoints;
    }

    public int getRoundsPlayed() {
        if (roundsPlayed < 0)
            throw new IllegalStateException();
        return roundsPlayed;
    }

    public double getMaxPoints() {
        if (maxPoints < 0)
            throw new IllegalStateException();
        return maxPoints;
    }

    public void setMaxPoints(double maxPoints) {
        this.maxPoints = maxPoints;
    }

    public double getWinrate() {
        if (winrate < 0)
            throw new IllegalStateException();
        return winrate;
    }

    public int getRank() {
        if (rank < 0)
            throw new IllegalStateException();
        return rank;
    }

    public void setRank(int rank) {
        if (rank <= 0)
            throw new IllegalArgumentException();
        this.rank = rank;
    }
}
