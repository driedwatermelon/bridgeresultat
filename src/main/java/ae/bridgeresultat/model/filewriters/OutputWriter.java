package ae.bridgeresultat.model.filewriters;

import ae.bridgeresultat.model.TableData;


public abstract class OutputWriter {

    final protected String name;
    final protected String date;
    final protected TableData table;
    final protected String path;
    final protected String fileName;

    protected OutputWriter(String name, String date, TableData table, String path, String fileName) {
        this.name = name;
        this.date = date;
        this.table = table;
        this.path = path;
        this.fileName = fileName;
    }

    public abstract void writeFile();
}
