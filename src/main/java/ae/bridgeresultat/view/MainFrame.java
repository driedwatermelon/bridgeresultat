package ae.bridgeresultat.view;

import ae.bridgeresultat.controller.Controller;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.swing.*;
import javax.swing.border.LineBorder;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.DefaultTableModel;

public class MainFrame {

    private final int TEAM_NUMBER = 50;
    private final int ROUND_NUMBER = 50;

    static final Color TOOLBAR_COLOR = new Color(215, 206, 199);
    private final Font LABEL_FONT = new Font("Verdana", Font.BOLD, 24);
    private final Font TEXT_FIELD_FONT = new Font("Arial", Font.PLAIN, 26);
    private final Font SPINNER_FONT = new Font("Courier New", Font.BOLD, 28);
    private final Font TABLE_HEADER_FONT = new Font("Arial", Font.BOLD, 24);
    private final Font TABLE_BODY_FONT = new Font("Arial", Font.PLAIN, 24);

    private final int scrollBarWidth = 22;

    private Controller controller;
	private JTable table;
	private JFrame frame;
	private JTextField titleTextField;
	private JSpinner dateSpinner;
	
	public MainFrame(Controller controller) {
	    this.controller = controller;
		frame = new JFrame("Bridge Resultat");
		frame.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
		
		addComponents(frame.getContentPane());
		
		frame.setPreferredSize(new Dimension(1260, 680));
		frame.setExtendedState(JFrame.MAXIMIZED_BOTH);
		frame.pack();
		frame.setLocationRelativeTo(null);
		frame.setVisible(true);
	}

	public JTable getTable() { return table; }

	public JFrame getFrame() { return frame; }

	public String getTitle() { return titleTextField.getText(); }

	public String getDate() {
	    Date date = (Date) dateSpinner.getValue();
	    return new SimpleDateFormat("dd.MM.yyyy").format(date);
	}
	
	private void addComponents(Container pane) {
		JPanel mainPanel = new JPanel();
		mainPanel.setLayout(new BorderLayout());

		mainPanel.add(createTopPanel(), BorderLayout.NORTH);
		mainPanel.add(createTableContainer(), BorderLayout.CENTER);
        mainPanel.add(new MainButtonPanel(controller), BorderLayout.SOUTH);

		pane.add(mainPanel);
	}

	private JPanel createTopPanel() {
        JPanel topPanel = new JPanel();
        topPanel.setLayout(new FlowLayout(FlowLayout.CENTER, 10, 30));
        topPanel.setBackground(TOOLBAR_COLOR);

        JLabel titleLabel = newToolbarLabel("Rubrik: ");
        topPanel.add(titleLabel);

        titleTextField = createTitleTextField();
        topPanel.add(titleTextField);

        JLabel dateLabel = newToolbarLabel("Datum: ");
        dateLabel.setBorder(BorderFactory.createEmptyBorder(0, 100, 0, 0));
        topPanel.add(dateLabel);

        dateSpinner = createDateSpinner();
        topPanel.add(dateSpinner);

        return topPanel;
    }

	private JLabel newToolbarLabel(String displayText) {
        JLabel titleLabel = new JLabel(displayText);
        titleLabel.setFont(LABEL_FONT);
        return titleLabel;
    }

    private JTextField createTitleTextField() {
        JTextField titleTextField = new JTextField();
        titleTextField.setPreferredSize(new Dimension(300, 40));
        titleTextField.setFont(TEXT_FIELD_FONT);
        titleTextField.setText("Bridgeprotokoll");
        return titleTextField;
    }

    private JSpinner createDateSpinner() {
        JSpinner dateSpinner = new JSpinner(new SpinnerDateModel());
        dateSpinner.setPreferredSize(new Dimension(200, 43));
        dateSpinner.setFont(SPINNER_FONT);
        JSpinner.DateEditor dateEditor = new JSpinner.DateEditor(dateSpinner, "dd.MM.yyyy");
        dateSpinner.setEditor(dateEditor);
        dateSpinner.setValue(new Date());
        return dateSpinner;
    }

    private JScrollPane createTableContainer() {
        DefaultTableModel dtm = createDTM();

        // Create a table with the model as a blueprint.
        table = createTable(dtm);

        // Add scrollbars to the table.
        table.setAutoResizeMode(JTable.AUTO_RESIZE_OFF);
        JScrollPane container = new JScrollPane(table);

        JScrollBar vertical = container.getVerticalScrollBar();
        JScrollBar horizontal = container.getHorizontalScrollBar();

        vertical.setPreferredSize(new Dimension(scrollBarWidth, Integer.MAX_VALUE));
        horizontal.setPreferredSize(new Dimension(Integer.MAX_VALUE, scrollBarWidth));

        return container;
    }

    private DefaultTableModel createDTM() {
	    // Add header.
        String[] header = new String[ROUND_NUMBER + 2];
        header[0] = "Nr.";
        header[1] = "LAG";
        for (int i = 1; i <= ROUND_NUMBER; i++)
            header[i + 1] = "" + i;
        DefaultTableModel dtm = new DefaultTableModel(header, 0);


        // Add rows.
        for (int i = 0; i < TEAM_NUMBER; i++) {
            String[] emptyRow = new String[ROUND_NUMBER + 2];
            emptyRow[0] = "" + (i + 1);

            for (int j = 0; j <= ROUND_NUMBER; j++) // note: <=
                emptyRow[j + 1] = "";

            dtm.addRow(emptyRow);
        }

        return dtm;
    }

    private JTable createTable(DefaultTableModel dtm) {
        // Create a table with the model as a blueprint.
        JTable table = new JTable(dtm);

        // Prevent reordering of the columns.
        table.getTableHeader().setReorderingAllowed(false);

        // Resize the columns.
        final int teamNumberColumnWidth = 60; // Minimum and maximum width.
        final int teamNameColumnMinWidth = 260; // Only minimum width.
        final int scoreColumnsWidth = 40; // Minimum and maximum width.

        table.getColumnModel().getColumn(0).setMinWidth(teamNumberColumnWidth);
        table.getColumnModel().getColumn(0).setMaxWidth(teamNumberColumnWidth);
        table.getColumnModel().getColumn(1).setMinWidth(teamNameColumnMinWidth);
        for (int i = 2; i < ROUND_NUMBER + 2; i++) {
            table.getColumnModel().getColumn(i).setMinWidth(scoreColumnsWidth);
            table.getColumnModel().getColumn(i).setMaxWidth(scoreColumnsWidth);
        }

        table.setRowHeight(34);
        table.setRowSelectionAllowed(false);

        // The JTable's cells' font depends on whether they are being edited.
        // Each cell has a font for "read-mode" and another font for "write-mode".
        // The code below applies the same font settings to both modes for a consistent look and feel.

        // Set the fonts for the table header and table body in "read-mode".
        // "read-mode" == table isn't being edited by the user
        table.getTableHeader().setFont(TABLE_HEADER_FONT);
        table.setFont(TABLE_BODY_FONT);

        // Center the number cells in "read-mode".
        DefaultTableCellRenderer cellRenderer = new DefaultTableCellRenderer();
        cellRenderer.setHorizontalAlignment(SwingConstants.CENTER);
        table.getColumnModel().getColumn(0).setCellRenderer(cellRenderer);
        for (int i = 2; i < table.getColumnCount(); i++) {
            table.getColumnModel().getColumn(i).setCellRenderer(cellRenderer);
        }

        // Set the font for the name cells in "write-mode".
        // "write-mode" == a table cell is being edited by the user.
        JTextField nameTextField = new JTextField();
        nameTextField.setFont(TABLE_BODY_FONT);
        nameTextField.setBorder(new LineBorder(Color.BLACK));
        DefaultCellEditor nameEditor = new DefaultCellEditor(nameTextField);
        table.getColumnModel().getColumn(1).setCellEditor(nameEditor);

        // Set the font for the number cells in "write-mode" and
        // center the text in the number cells.
        JTextField numberTextField = new JTextField();
        numberTextField.setHorizontalAlignment(JTextField.CENTER);
        numberTextField.setFont(TABLE_BODY_FONT);
        numberTextField.setBorder(new LineBorder(Color.BLACK));
        DefaultCellEditor numberEditor = new DefaultCellEditor(numberTextField);
        table.getColumnModel().getColumn(0).setCellEditor(numberEditor);
        for (int i = 2; i < table.getColumnCount(); i++) {
            table.getColumnModel().getColumn(i).setCellEditor(numberEditor);
        }

        return table;
    }
}
