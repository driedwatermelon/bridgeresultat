package ae.bridgeresultat.model;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class TableData {

    private List<TeamData> records = new ArrayList<>();
    private String ignoreSymbol;
    private double[] columnSums;
    private double[] maximums; // The maximum amount of points a team can earn each round.

    public void setIgnoreSymbol(String ignoreSymbol) {
        this.ignoreSymbol = ignoreSymbol;
    }

    public void addRecord(TeamData record) {
        records.add(record);
    }

    public List<TeamData> getRecords() {
        return records;
    }

    public double[] getColumnSums() { return columnSums; }

    public int getRecordAmount() { return records.size(); }

    public int getRoundAmount() {
        if (records.isEmpty()) return 0;
        return records.get(0).getTotalRounds();
    }

    public int getTotalRounds() {
        if (records.isEmpty()) return 0;
        return records.get(0).getTotalRounds();
    }

    public void calculateRecords() throws InvalidFieldException {
        calculateColumnSums();

        for (TeamData record : records) record.calculatePoints(ignoreSymbol);

        calculateMaxEveryRound();

        for (TeamData record : records) {
            calculateMaxPoints(record);
            record.calculateWinrate();
        }

        Collections.sort(records);
        calculateRanks();
    }

    private void calculateColumnSums() {
        if (records.isEmpty()) return;
        if (!columnAmountEven()) throw new IllegalStateException();
        double[] result = new double[records.get(0).getTotalRounds()];

        for (int col = 0; col < result.length; col++) {
            result[col] = 0;
            for (int row = 0; row < records.size(); row++) {
                result[col] += records.get(row).getPointsForRound(col);
            }
        }

        columnSums = result;
    }

    private boolean columnAmountEven() {
        if (records.isEmpty()) return true;

        int referenceLength = getTotalRounds();
        for (TeamData record : records)
            if (record.getPointsEachRound().length != referenceLength)
                return false;
        return true;
    }

    private void calculateMaxEveryRound() {
        maximums = new double[columnSums.length];
        for (int i = 0; i < columnSums.length; i++) {
            maximums[i] = columnSums[i] * 2 / participatingTeams(i);
        }
    }

    private int participatingTeams(int round) {
        int amount = 0;
        for (TeamData record : records) {
            if (record.participatedInRound(round, ignoreSymbol)) amount++;
        }
        return amount;
    }

    public void calculateMaxPoints(TeamData record) {
        double max = 0;
        for (int i = 0; i < columnSums.length; i++) {
            if (record.participatedInRound(i, ignoreSymbol)) {
                max += maximums[i];
            }
        }
        record.setMaxPoints(max);
    }

    private void calculateRanks() {
        if (records.isEmpty()) return;

        records.get(0).setRank(1);
        for (int i = 1; i < records.size(); i++) {
            if (records.get(i).getWinrate() == (records.get(i - 1).getWinrate())) {
                records.get(i).setRank(records.get(i - 1).getRank());
            } else {
                records.get(i).setRank(i + 1);
            }
        }
    }

    public int totalPoints() {
        int sum = 0;
        for (TeamData record : records) {
            if (record.getTotalPoints() == -1) return -1;
            sum += record.getTotalPoints();
        }
        if (sum == 0) sum = -1;
        return sum;
    }

    @Override
    public String toString() {
        String result = "";
        for (TeamData record : records) {
            result += record + "\n";
        }
        return result;
    }

    public boolean validateColumnSums() {
        if (columnSums == null) return true;

        for (int i = 1; i < columnSums.length; i++) {
            if (columnSums[i] != columnSums[i - 1]) return false;
        }

        return true;
    }
}
